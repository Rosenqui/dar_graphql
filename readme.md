1)
Add the ability to add a new employee.

DAR Notes:
This was achieved by setting up a mutation. supplied parameters include name, department, and salary. Departments must exist prior to employee creation otherwise
the user will not have an associated department.

	mutation {
	  createEmployee(departmentName:"IT",name:"Adam R",salary:80000) {
      employee {
        name
        hiredOn
        salary
        department {
          name
          id
        }
      }
	  }
	}

DAR Notes:
To see available departments, run the following query:

{
  allDepartments {
    edges {
      node {
        name
      }
    }
  }
}    


2)
Add the ability to list all existing employees, with their name, id, department name and
salary.

DAR Notes:
This was available with the demo code, but required changes to the underlying schema/models for salary fields on the employee
Run the following query to see a list of all employees with their respective information

{
  allEmployees{
    edges {
      node{
        name
        salary
        department {
          name
        }
      }
    }
  }
}


3)
Change the list entry point to show the employees page by page. This would be used in
a UI. Some features we’re looking for are :

DAR Notes:
- No known solution when using baked in cursor based pagination since you only get back next/last page pointers
-- have currently been banging my head against my keyboard nightly trying to figure out how to get query parms from within a custom resolver on my connection class. Have figured out how to add fields in the response (see total_count) but not how to extract the options passed in via the query to do the math server side.

    a)
    Ability to see total number of pages in any given page
    b)
    Ability to see in which page I am right now.
    c)
    Ability to goto Nth page
    d)
    Ability to sort by department name and department salary in a paginated manner

In any event, answers to the above questions are escaping me with custom solution. Using the available cursor pointers, and the available total count field on the connection, you can kind of get same-ish results. 

If the following is your base query:

{ 
  allEmployeesOptions(first:2,sort:salary_asc){
    totalCount
    pageInfo {
      startCursor
      endCursor
      hasNextPage
      hasPreviousPage
    }
    edges {
      node {
        id
        name
        salary
        department {
          id
          name
        }
      }
    }
  }
}

You will get back the pointer information that can be used for pagination, so follow up queries can utilize the "before" or "after" parameters in conjunction with the values in the last set of results. For example, if the following cursor information was returned...

"allEmployeesOptions": {
      "totalCount": 4,
      "pageInfo": {
        "startCursor": "YXJyYXljb25uZWN0aW9uOjA=",
        "endCursor": "YXJyYXljb25uZWN0aW9uOjE=",
        "hasNextPage": true,
        "hasPreviousPage": false
      },

The follow up query to get the next page of results would look like so:

{ 
  allEmployeesOptions(first:2,after:"YXJyYXljb25uZWN0aW9uOjE=",sort:salary_asc){
    totalCount
    pageInfo {
      startCursor
      endCursor
      hasNextPage
      hasPreviousPage
    }
    edges {
      node {
        id
        name
        salary
        department {
          id
          name
        }
      }
    }
  }
}

This would return you the next two records.

Since the # of rows returned can typically be controlled from a front end UI, by getting the total count value you can calculate the number of pages after you request your initial data. I.E, psuedo logic

- Query Employees, request first:10
- 10 records returned and total count value == "39"
- total # pages == math.ceil(total_count / number_records_requested)

In conjunction with the cursors, you can efficiently query each page of results in a next/previous fashion. If you were to store the pointers you could dynamically jump from page to page as well.

=== TESTING ===

For all tests to execute successfully, you will need to ensure that the server is up and running. It is typically bad practice to start/stop webserver instances as part of a test case, so i advise running the server in another python instance.

    -NOTE-
    * This will only affect the two test cases that communicate with the server over HTTP
    * The existing test cases do not require the server to be up and running.

To execute the tests, run the following command at dos/shell while in the top level project folder:

    "python -m unittest tests/tests.py"


=== Using the UI ===

Quick and dirty front end has been made to submit queries into the graphql interface from some form of UI.

To view functionality, go to localhost:8081/
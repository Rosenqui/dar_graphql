from models import Employee
from schema import schema
import snapshottest
from graphene.test import Client
from setup import setup
import requests

client = Client(schema)

class httptests(snapshottest.TestCase):
    def setUp(self):
        setup()

    def test_send_get_all_dept(self):
        resp = requests.get("http://localhost:8081/graphql?query={allDepartments {edges {node {name}}}}")
        self.assertMatchSnapshot(resp.json())

    def test_send_post_all_dept(self):
        resp = requests.post("http://localhost:8081/graphql",json="{allDepartments {edges {node {name}}}}")
        self.assertMatchSnapshot(resp.json())


class modeltests(snapshottest.TestCase):
    def setUp(self):
        setup()
        pass

    def test_insert_employee(self):
        query = '''mutation {
	              createEmployee(departmentName:"IT",name:"John Doe",salary:55000) {
                  employee {
                    name
                    salary
                    department {
                      name
                      id
                    }
                  }
                }
              }'''
        execute = client.execute(query)
        self.assertMatchSnapshot(execute)

    def test_insert_employee_no_dept(self):
        query = '''mutation {
              createEmployee(departmentName:"",name:"John N Dept.",salary:35000) {
                employee {
                  name
                  salary
                  department {
                    name
                    id
                  }
                }
              }
            }'''
        execute = client.execute(query)
        self.assertMatchSnapshot(execute)

    def test_get_departments(self):
        query = '''{
                      allDepartments {
                        edges {
                          node {
                            name
                          }
                        }
                      }
                    }'''
        execute = client.execute(query)
        self.assertMatchSnapshot(execute)

    '''
      This function will return different results each time based on the inserts,
    '''

    def test_get_all_employees(self):
        query = '''{
                    allEmployees{
                      edges {
                        node{
                          id
                          name
                          salary
                          department {
                            id
                            name
                          }
                        }
                      }
                    }
                  }'''
        execute = client.execute(query)
        self.assertMatchSnapshot(execute)


if __name__ == '__main__':
    unittest.main()

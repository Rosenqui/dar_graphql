# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['httptests::test_send_get_all_dept 1'] = {
    'data': {
        'allDepartments': {
            'edges': [
                {
                    'node': {
                        'name': 'IT'
                    }
                },
                {
                    'node': {
                        'name': 'Sales'
                    }
                },
                {
                    'node': {
                        'name': 'HR'
                    }
                },
                {
                    'node': {
                        'name': 'QA'
                    }
                }
            ]
        }
    }
}

snapshots['httptests::test_send_post_all_dept 1'] = {
    'errors': [
        {
            'message': 'GraphQL params should be a dict. Received {allDepartments {edges {node {name}}}}.'
        }
    ]
}

snapshots['modeltests::test_get_all_employees 1'] = {
    'data': {
        'allEmployees': {
            'edges': [
                {
                    'node': {
                        'department': {
                            'id': 'RGVwYXJ0bWVudDox',
                            'name': 'IT'
                        },
                        'id': 'RW1wbG95ZWU6MQ==',
                        'name': 'Adam Rosenquist',
                        'salary': 75000
                    }
                },
                {
                    'node': {
                        'department': {
                            'id': 'RGVwYXJ0bWVudDox',
                            'name': 'IT'
                        },
                        'id': 'RW1wbG95ZWU6Mg==',
                        'name': 'Kenn Belford',
                        'salary': 75000
                    }
                },
                {
                    'node': {
                        'department': {
                            'id': 'RGVwYXJ0bWVudDox',
                            'name': 'IT'
                        },
                        'id': 'RW1wbG95ZWU6Mw==',
                        'name': 'Jeff Holtham',
                        'salary': 75000
                    }
                },
                {
                    'node': {
                        'department': {
                            'id': 'RGVwYXJ0bWVudDo0',
                            'name': 'QA'
                        },
                        'id': 'RW1wbG95ZWU6NA==',
                        'name': 'Karen T-G',
                        'salary': 75000
                    }
                },
                {
                    'node': {
                        'department': {
                            'id': 'RGVwYXJ0bWVudDoy',
                            'name': 'Sales'
                        },
                        'id': 'RW1wbG95ZWU6NQ==',
                        'name': 'John Doe',
                        'salary': 75000
                    }
                },
                {
                    'node': {
                        'department': {
                            'id': 'RGVwYXJ0bWVudDoz',
                            'name': 'HR'
                        },
                        'id': 'RW1wbG95ZWU6Ng==',
                        'name': 'Jane Buck',
                        'salary': 75000
                    }
                }
            ]
        }
    }
}

snapshots['modeltests::test_get_departments 1'] = {
    'data': {
        'allDepartments': {
            'edges': [
                {
                    'node': {
                        'name': 'IT'
                    }
                },
                {
                    'node': {
                        'name': 'Sales'
                    }
                },
                {
                    'node': {
                        'name': 'HR'
                    }
                },
                {
                    'node': {
                        'name': 'QA'
                    }
                }
            ]
        }
    }
}

snapshots['modeltests::test_insert_employee 1'] = {
    'data': {
        'createEmployee': {
            'employee': {
                'department': {
                    'id': 'RGVwYXJ0bWVudDox',
                    'name': 'IT'
                },
                'name': 'John Doe',
                'salary': 55000
            }
        }
    }
}

snapshots['modeltests::test_insert_employee_no_dept 1'] = {
    'data': {
        'createEmployee': {
            'employee': {
                'department': None,
                'name': 'John N Dept.',
                'salary': 35000
            }
        }
    }
}

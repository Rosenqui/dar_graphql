# flask_sqlalchemy/schema.py
import logging
import graphene
from graphene import relay, Mutation
from graphene_sqlalchemy import SQLAlchemyObjectType, SQLAlchemyConnectionField
from models import db_session, Department as DepartmentModel, Employee as EmployeeModel
from sqlalchemy import func

class Department(SQLAlchemyObjectType):
    class Meta:
        model = DepartmentModel
        interfaces = (relay.Node, )


class DepartmentConnection(relay.Connection):
    class Meta:
        node = Department

class Employee(SQLAlchemyObjectType):
    class Meta:
        model = EmployeeModel
        interfaces = (relay.Node, )


class EmployeeConnectionDAR(relay.Connection):
    total_count = graphene.Int()
    class Meta:
        node = Employee
    def resolve_total_count(self,info):
        return db_session.query(func.count(EmployeeModel.id)).scalar()

class CreateEmployee(graphene.Mutation):
    class Arguments:
        name = graphene.String()
        departmentName = graphene.String()
        salary = graphene.Int()
        
    employee = graphene.Field(Employee)

    def mutate(self, info, name, departmentName,salary=80000):
        department = DepartmentModel(name=departmentName)

        query = Department.get_query(info)

        deptobj = query.filter(DepartmentModel.name == departmentName).first()
        employee = EmployeeModel(
                    name=name,
                    department=deptobj,
                    salary=salary)
        db_session.add(employee)
        db_session.commit()
        return CreateEmployee(employee=employee)
    
class myMutations(graphene.ObjectType):
    create_employee = CreateEmployee.Field()

class Query(graphene.ObjectType):
    node = relay.Node.Field()
    # Allows sorting over multiple columns, by default over the primary key
    all_employees = SQLAlchemyConnectionField(EmployeeConnectionDAR)
    # Disable sorting over this field
    all_departments = SQLAlchemyConnectionField(DepartmentConnection, sort=None)
    #
    all_employees_options = SQLAlchemyConnectionField(EmployeeConnectionDAR,limit=graphene.Int(),offset=graphene.Int(),sortField=graphene.String())

schema = graphene.Schema(query=Query,mutation=myMutations)
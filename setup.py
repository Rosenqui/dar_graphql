from models import engine, db_session, Base, Department, Employee


def setup():
    # clean up any existing tables/records, so we can create a fresh base setof records.
    Base.metadata.drop_all(bind=engine)
    # create the table definitions
    Base.metadata.create_all(bind=engine)

    # create some departments
    itdept = Department(name="IT")
    db_session.add(itdept)
    salesdept = Department(name="Sales")
    db_session.add(salesdept)
    hrdept = Department(name="HR")
    db_session.add(hrdept)
    qadept = Department(name="QA")
    db_session.add(qadept)

    # create some employees, with departments
    emp = Employee(name="Adam Rosenquist", department=itdept)
    db_session.add(emp)
    emp = Employee(name="Kenn Belford", department=itdept)
    db_session.add(emp)
    emp = Employee(name="Jeff Holtham", department=itdept)
    db_session.add(emp)
    emp = Employee(name="Karen T-G", department=qadept)
    db_session.add(emp)
    emp = Employee(name="John Doe", department=salesdept)
    db_session.add(emp)
    emp = Employee(name="Jane Buck", department=hrdept)
    db_session.add(emp)

    # finally save 'em all
    db_session.commit()

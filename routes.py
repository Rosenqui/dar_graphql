import json
import logging

from flask import Flask, render_template, request, jsonify
from waitress import serve
from flask_graphql import GraphQLView

# self includes    
from models import db_session
from schema import schema, Department
from setup import setup

def setupLogging():
    logging.basicConfig(filename="./logs/log.log", level=logging.INFO)

FLASKAPP = Flask(__name__)

FLASKAPP.add_url_rule(
    '/graphql',
    view_func=GraphQLView.as_view(
        'graphql',
        schema=schema,
        graphiql=True # for having the GraphiQL interface
    )
)

@FLASKAPP.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()

@FLASKAPP.route("/", methods=['POST','GET'])
def ui():
    return render_template('main.html')

def wsgi():
    setupLogging()
    logging.info("initializing database...")
    setup()
    logging.info("Starting wsgi...")
    # use waitress rather than crappy flask dev server to host it.
    serve(FLASKAPP, port='8081')

if __name__ == '__main__':
    wsgi()